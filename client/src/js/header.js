function toggleScrollClass() {
   let nav = document.getElementsByClassName('header-wrapper')[0];
   const wrap = document.getElementsByClassName('header-wrapper')[0];
   window.pageYOffset >= 1 ? nav.classList.add('header-wrapper--is-scroll') : nav.classList.remove('header-wrapper--is-scroll')
 }
 
 window.addEventListener('scroll', toggleScrollClass);
 toggleScrollClass();
